#ifndef UI_FORMULACHECKER_H
#define UI_FORMULACHECKER_H

#include <QWidget>

namespace Ui {
class UI_FormulaChecker;
}

class UI_FormulaChecker : public QWidget
{
    Q_OBJECT

public:
    explicit UI_FormulaChecker(QWidget *parent = 0);
    ~UI_FormulaChecker();

private slots:
    void onCheck();
    void onNewLog(QtMsgType type, const QString &msg);

private:
    Ui::UI_FormulaChecker *ui;
};

#endif // UI_FORMULACHECKER_H
