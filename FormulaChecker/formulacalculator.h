#ifndef FORMULACALCULATOR_H
#define FORMULACALCULATOR_H

#include <qstring.h>
#include <QList>
#include <QtGlobal>

//当前结点的类型，用于折叠表达式时进行判断
enum FormulaNodeType
{
    FNT_VALUE,          //值结点，只能为叶子结点
    FNT_OPERATOR,       //通用运算符结点，可通过左右支树计算出值
    FNT_LEFT_BRACKET,   //左括号结点，仅点位用，队列中等待右括号的折叠
    FNT_QUESTION_MARK,  //问号结点，与后续的冒号结点折叠
    FNT_COLON_MARK,     //冒号结点，与之前的问题结点折叠
    FNT_SUB_FORMUAL     //折叠后的结点，相当于值结点
};

//计算优先级，值越小越高
enum FormulaPriorityType
{
    FPT_VALUE = 1,
    FPT_MULTI,
    FPT_PLUS,
    FPT_EQUAL,
    FPT_AND,
    FPT_QUESTION_MARK,
    FPT_MAX
};

class FormulaNode
{
public:
    FormulaNode()
    {
        m_leftNode = NULL;
        m_rightNode = NULL;
    }

    virtual ~FormulaNode() {}
    
    virtual float calc() = 0;
    
    int setLeftNode(FormulaNode *node)
    {
        if(m_leftNode)
        {
            qDebug("FormulaNode::setLeftNode: already have a left node.");
            return -1;
        }
        m_leftNode = node;

        return 0;
    }
    int setRightNode(FormulaNode *node)
    {
        if(m_rightNode)
        {
            qDebug("FormulaNode::setRightNode: already have a right node.");
            return -1;
        }
        m_rightNode = node;

        return 0;
    }
    
    void setCurrentEquipId(uint equipId, uint mbmsId = 0)
    {
        m_curEquipId = equipId;
        m_curMbmsId = mbmsId;
        if(m_leftNode)
        {
            m_leftNode->setCurrentEquipId(equipId, mbmsId);
        }
        if(m_rightNode)
        {
            m_rightNode->setCurrentEquipId(equipId, mbmsId);
        }
    }

    float leftValue()
    {
        return m_leftNode->calc();
    }

    float rightValue()
    {
        return m_rightNode->calc();
    }
    
public:
    FormulaPriorityType priority;
    FormulaNodeType type;
protected:
    FormulaNode *m_leftNode;
    FormulaNode *m_rightNode;
    uint m_curEquipId;
    uint m_curMbmsId;
};

enum FValueType
{
    FVT_CONST = 0,
    FVT_SIGNAL,
    FVT_EQUIP_ID
};

enum tagEquipId
{
    BMU_EQUIP_TYPE_ID  = 0x8001,
    MBMS_EQUIP_TYPE_ID,
    BAMS_EQUIP_TYPE_ID,

    /**
     * 此处不要作修改
     */
    MIN_EQUIP_TYPE_ID = BMU_EQUIP_TYPE_ID,
    MAX_EQUIP_TYPE_ID = BAMS_EQUIP_TYPE_ID,
};

class FValueNode : public FormulaNode
{
public:
    FValueNode(float value = 0);
    FValueNode(uint equipTypeId, uint equipId, uint signalId, uint mbmsId = 0);
    FValueNode(tagEquipId equipTypeId);
    
    float calc();
private:
    FValueType m_valueType;
    
    float m_value;
    
    uint m_equipTypeId;
    uint m_equipId;
    uint m_signalId;
    uint m_mbmsId;

    uint m_equipType;
};

//运算符结点的类型，决定了该结点的计算方式
enum FormulaOperatorType
{
    FOT_PLUS,
    FOT_MINUS,
    FOT_MULTI,
    FOT_DIVIDE,
    FOT_MOD,
    FOT_EQUAL,
    FOT_NOT_EQUAL,
    FOT_AND,
    FOT_OR,
    FOT_MORE_THAN,
    FOT_LESS_THAN,
    FOT_QUESTION_MARK,
    FOT_COLON_MARK
};

class FOperNode : public FormulaNode
{
public:
    FOperNode(FormulaOperatorType formulaOperatorType = FOT_PLUS);
    ~FOperNode();
    
    float calc();
private:
    FormulaOperatorType m_operatorType;
};

class FLeftBracket : public FormulaNode
{
public:
    FLeftBracket();
    
    float calc()
    {
        qDebug("FLeftBracket::calc: error to calc left brcket.");
        return 0;
    }
};

/**
 * @brief The FormulaCalculator class: 表达式解析类，用于解析算术字符串
 */
class FormulaCalculator
{
public:
    FormulaCalculator();
    
    /**
     * @brief parseFormula: 解析表达式接口
     * @param formula：被解析的表达式字符串
     * @return NULL: 失败；非NULL,解析所得的可用于计算的表达式结构体
     */
    FormulaNode *parseFormula(const QString &formula);
private:
    //折叠右括号前的子表达式
    int mergeParenthesisFormula(QList<FormulaNode *> &nodeStack);
    //折叠问号前的子表达式，作为问号运算符的左树
    int mergeQuestionMarkFormula(QList<FormulaNode *> &nodeStack);
    //折叠冒号前的子表达式，作为当前冒号的左树或之前冒号的右树
    int mergeColonMarkFormula(QList<FormulaNode *> &nodeStack);
    //折叠包含问题与冒号运算符的子表达式
    int mergeQuestionEndFormula(QList<FormulaNode *> &nodeStack);
    //将整个nodeStack折叠为一个结点
    int mergeFormulaToOneNode(QList<FormulaNode *> &nodeStack);

    FormulaNode *mergeFormula(QList<FormulaNode *> &nodeStack);
    FormulaNode *parseSignalNode(const QByteArray &formulaStr, int &index);
    FormulaNode *parseValueNode(const QByteArray &formulaStr, int &index);
};

#endif // FORMULACALCULATOR_H
