#include "formulacalculator.h"
#include <math.h>

#define CHECK_LAST_NODE_FORBIDDEN_TYPE(lastForbidden, current) if(lastNodeType == lastForbidden) \
{ \
    goto Parse_formula_error; \
} \
else \
{ \
    lastNodeType = current; \
}

#define CHECK_LAST_NODE_MUST_TYPE(lastMust, current) if(lastNodeType != lastMust) \
{ \
    goto Parse_formula_error; \
} \
else \
{ \
    lastNodeType = current; \
}

FValueNode::FValueNode(float value)
{
    type = FNT_VALUE;
    priority = FPT_VALUE;
    m_valueType = FVT_CONST;
    
    m_value = value;
}

FValueNode::FValueNode(uint equipTypeId, uint equipId, uint signalId, uint mbmsId)
{
    type = FNT_VALUE;
    priority = FPT_VALUE;
    m_valueType = FVT_SIGNAL;
    
    m_equipTypeId = equipTypeId;
    m_equipId = equipId;
    m_signalId = signalId;
    m_mbmsId = mbmsId;
}

FValueNode::FValueNode(tagEquipId equipTypeId)
{
    type = FNT_VALUE;
    priority = FPT_VALUE;
    m_valueType = FVT_EQUIP_ID;

    m_equipType = equipTypeId;
}

float FValueNode::calc()
{
    switch (m_valueType)
    {
    case FVT_SIGNAL:
#if USING_TRUE_SIGNAL_SYSTEM
        {
            SYS_SIG_VAL sigVal;
            CSignalSystemMgr *sigSysMgr = CSignalSystemMgr::Instance();
            int equipId = 1, mbmsId = 0;

            if(m_equipId == 0)
            {
                equipId = m_curEquipId;
                mbmsId = m_curMbmsId;
            }
            else
            {
                equipId = m_equipId;
                mbmsId = m_mbmsId;
            }

            if(Pw_OK != sigSysMgr->GetSigVal(m_equipTypeId, equipId, m_signalId, sigVal, mbmsId))
            {
                QLOG_ERROR() << "FValueNode::calc: failed to get signal value of: "
                             << m_equipTypeId
                             << ", "
                             << equipId
                             << ", "
                             << m_signalId
                             << "; will treat as 0";
                return 0;
            }

            switch(sigVal.type)
            {
            case PW_FLOAT:
                m_value = sigVal.Value.f32Value;
                break;
            case PW_UINT:
                m_value = (float)(sigVal.Value.u32Value);
                break;
            case PW_INT:
                m_value = (float)(sigVal.Value.s32Value);
                break;
            default:
                QLOG_ERROR() << "FValueNode::calc: unsupport signal value type of: "
                             << m_equipTypeId
                             << ", "
                             << equipId
                             << ", "
                             << m_signalId
                             << "; will treat as 0";
                m_value = 0;
                break;
            }

            return m_value;
//        return 1025;
        }
#else
        qDebug("FValueNode::calc: fake signal node, will return 0.");
        return 0;
#endif
        break;
    case FVT_CONST:
        return m_value;
        break;
    case FVT_EQUIP_ID:
        switch (m_equipType)
        {
        case MBMS_EQUIP_TYPE_ID:
            return m_curMbmsId == 0 ? m_curEquipId : m_curMbmsId;
            break;
        case BMU_EQUIP_TYPE_ID:
            return m_curEquipId;
            break;
        default:
            return 1;
            break;
        }
        break;
    }

    return 0;
}

FOperNode::FOperNode(FormulaOperatorType formulaOperatorType)
{
    type = FNT_OPERATOR;
    switch (formulaOperatorType)
    {
    case FOT_PLUS:
    case FOT_MINUS:
        priority = FPT_PLUS;
        break;
    case FOT_MULTI:
    case FOT_DIVIDE:
    case FOT_MOD:
        priority = FPT_MULTI;
        break;
    case FOT_EQUAL:
    case FOT_NOT_EQUAL:
    case FOT_MORE_THAN:
    case FOT_LESS_THAN:
        priority = FPT_EQUAL;
        break;
    case FOT_AND:
    case FOT_OR:
        priority = FPT_AND;
        break;
    case FOT_QUESTION_MARK:
        type = FNT_QUESTION_MARK;
        priority = FPT_QUESTION_MARK;
        break;
    case FOT_COLON_MARK:
        type = FNT_COLON_MARK;
        priority = FPT_QUESTION_MARK;
        break;
    default:
        qDebug("FOperNode::FOperNode: unsupport operator.");
        break;
    }
    
    this->m_operatorType = formulaOperatorType;
}

FOperNode::~FOperNode()
{
    if(this->m_leftNode)
    {
        delete this->m_leftNode;
    }
    if(this->m_rightNode)
    {
        delete this->m_rightNode;
    }
}

float FOperNode::calc()
{
    float value = 0;
    float left = m_leftNode->calc();
    float right = this->m_operatorType == FOT_QUESTION_MARK ? 0 : m_rightNode->calc();
    
    switch (this->m_operatorType)
    {
    case FOT_PLUS:
        value = left + right;
        break;
    case FOT_MINUS:
        value = left - right;
        break;
    case FOT_MULTI:
        value = left * right;
        break;
    case FOT_DIVIDE:
        value = left / right;
        break;
    case FOT_MOD:
        value = (uint)left % (uint)right;
        break;
    case FOT_EQUAL:
        value = fabs(left - right) > 0.00001 ? 0 : 1;
        break;
    case FOT_NOT_EQUAL:
        value = fabs(left - right) > 0.00001 ? 1 : 0;
        break;
    case FOT_MORE_THAN:
        value = left > right ? 1 : 0;
        break;
    case FOT_LESS_THAN:
        value = left < right ? 1 : 0;
        break;
    case FOT_AND:
        value = ((uint)(left) && (uint)(right)) ? 1 : 0;
        break;
    case FOT_OR:
        value = ((uint)(left) || (uint)(right)) ? 1 : 0;
        break;
    case FOT_QUESTION_MARK:
        value = left ? m_rightNode->leftValue() : m_rightNode->rightValue();
        break;
    case FOT_COLON_MARK:
        qDebug("FOperNode::calc: must be an error to calc colon operator.");
        break;
    default:
        qDebug("FOperNode::calc: unsupport operator.");
        break;
    }
    
    return value;
}

FLeftBracket::FLeftBracket()
{
    type = FNT_LEFT_BRACKET;
}

FormulaCalculator::FormulaCalculator()
{

}

FormulaNode *FormulaCalculator::parseFormula(const QString &formula)
{
    int ret = 0;
    int index = 0;
    QList<FormulaNode *> nodeList;
    FormulaNode *newNode = NULL;
    FormulaNodeType lastNodeType = FNT_OPERATOR;
    QByteArray formulaStr = formula.simplified().toAscii();
    
    //解析字符串中的算术符号关键字，以及数据或变量，添加到nodeList列表中
    for(index = 0; index < formulaStr.length(); ++index)
    {
        switch (formulaStr.at(index))
        {
        case ' ':
            // 跳过空白字符串
            break;
        case '(':
            CHECK_LAST_NODE_FORBIDDEN_TYPE(FNT_VALUE, FNT_LEFT_BRACKET)
            nodeList.append(new FLeftBracket());
            break;
        case ')':
            CHECK_LAST_NODE_FORBIDDEN_TYPE(FNT_OPERATOR, FNT_VALUE)
            //遇到右括号，则将括号内的子表达式进行折叠
            ret = mergeParenthesisFormula(nodeList);
            if(ret < 0)
            {
                goto Parse_formula_error;
            }
            break;
        case '+':
            CHECK_LAST_NODE_MUST_TYPE(FNT_VALUE, FNT_OPERATOR)
            nodeList.append(new FOperNode(FOT_PLUS));
            break;
        case '-':
            CHECK_LAST_NODE_MUST_TYPE(FNT_VALUE, FNT_OPERATOR)
            nodeList.append(new FOperNode(FOT_MINUS));
            break;
        case '*':
            CHECK_LAST_NODE_MUST_TYPE(FNT_VALUE, FNT_OPERATOR)
            nodeList.append(new FOperNode(FOT_MULTI));
            break;
        case '/':
            CHECK_LAST_NODE_MUST_TYPE(FNT_VALUE, FNT_OPERATOR)
            nodeList.append(new FOperNode(FOT_DIVIDE));
            break;
        case '%':
            CHECK_LAST_NODE_MUST_TYPE(FNT_VALUE, FNT_OPERATOR)
            nodeList.append(new FOperNode(FOT_MOD));
            break;
        case '=':
            CHECK_LAST_NODE_MUST_TYPE(FNT_VALUE, FNT_OPERATOR)
            if((index + 1 < formulaStr.length()) &&
                    (formulaStr.at(++index) == '='))
            {
                nodeList.append(new FOperNode(FOT_EQUAL));
            }
            else
            {
                goto Parse_formula_error;
            }
            break;
        case '!':
            CHECK_LAST_NODE_MUST_TYPE(FNT_VALUE, FNT_OPERATOR)
            if((index + 1 < formulaStr.length()) &&
                    (formulaStr.at(++index) == '='))
            {
                nodeList.append(new FOperNode(FOT_NOT_EQUAL));
            }
            else
            {
                goto Parse_formula_error;
            }
            break;
        case '>':
            CHECK_LAST_NODE_MUST_TYPE(FNT_VALUE, FNT_OPERATOR)
            nodeList.append(new FOperNode(FOT_MORE_THAN));
            break;
        case '<':
            CHECK_LAST_NODE_MUST_TYPE(FNT_VALUE, FNT_OPERATOR)
            nodeList.append(new FOperNode(FOT_LESS_THAN));
            break;
        case '&':
            CHECK_LAST_NODE_MUST_TYPE(FNT_VALUE, FNT_OPERATOR)
            if((index + 1 < formulaStr.length()) &&
                    (formulaStr.at(++index) == '&'))
            {
                nodeList.append(new FOperNode(FOT_AND));
            }
            else
            {
                goto Parse_formula_error;
            }
            break;
        case '|':
            CHECK_LAST_NODE_MUST_TYPE(FNT_VALUE, FNT_OPERATOR)
            if((index + 1 < formulaStr.length()) &&
                    (formulaStr.at(++index) == '|'))
            {
                nodeList.append(new FOperNode(FOT_OR));
            }
            else
            {
                goto Parse_formula_error;
            }
            break;
        case '?':
            CHECK_LAST_NODE_MUST_TYPE(FNT_VALUE, FNT_OPERATOR)
            ret = mergeQuestionMarkFormula(nodeList);
            if(ret < 0)
            {
                goto Parse_formula_error;
            }
            break;
        case ':':
            CHECK_LAST_NODE_MUST_TYPE(FNT_VALUE, FNT_OPERATOR)
            ret = mergeColonMarkFormula(nodeList);
            if(ret < 0)
            {
                goto Parse_formula_error;
            }
            break;
        case 's':
            CHECK_LAST_NODE_FORBIDDEN_TYPE(FNT_VALUE, FNT_VALUE)
            newNode = parseSignalNode(formulaStr, index);
            if(newNode == NULL)
            {
                goto Parse_formula_error;
            }
            else
            {
                nodeList.append(newNode);
            }
            break;
        case 'm':
            CHECK_LAST_NODE_FORBIDDEN_TYPE(FNT_VALUE, FNT_VALUE)
            nodeList.append(new FValueNode(MBMS_EQUIP_TYPE_ID));
            break;
        case 'b':
            CHECK_LAST_NODE_FORBIDDEN_TYPE(FNT_VALUE, FNT_VALUE)
            nodeList.append(new FValueNode(BMU_EQUIP_TYPE_ID));
            break;
        default:
            CHECK_LAST_NODE_FORBIDDEN_TYPE(FNT_VALUE, FNT_VALUE)
            newNode = parseValueNode(formulaStr, index);
            if(newNode == NULL)
            {
                goto Parse_formula_error;
            }
            else
            {
                nodeList.append(newNode);
            }
            break;
        }
    }
    
    CHECK_LAST_NODE_FORBIDDEN_TYPE(FNT_OPERATOR, FNT_VALUE)
    
    if(mergeFormulaToOneNode(nodeList) != 0)
    {
        goto Parse_formula_error;
    }
    else
    {
        return nodeList.first();
    }
    
Parse_formula_error:
    foreach (FormulaNode *node, nodeList)
    {
        delete node;
    }
    
    qDebug("FormulaCalculator::parseFormula: parse formula: \"%s\", error at : %d.", formulaStr.constData(), index);
    
    return NULL;
}

int FormulaCalculator::mergeParenthesisFormula(QList<FormulaNode *> &nodeStack)
{
    QList <FormulaNode *> subFormulaNode;
    FormulaNode *mergeNode = NULL;
    
    //从后往前找子表达式，然后折叠子表达式
    while(! nodeStack.isEmpty())
    {
        switch (nodeStack.last()->type)
        {
        case FNT_LEFT_BRACKET:
            //找到左括号，则折叠子表达式，替换左括号
            delete nodeStack.takeLast();
            mergeNode = mergeFormula(subFormulaNode);
            if(mergeNode == NULL)
            {
                foreach (FormulaNode *node, subFormulaNode)
                {
                    delete node;
                }
                return -1;
            }
            else
            {
                nodeStack.append(mergeNode);
                return 0;
            }
            break;
        case FNT_COLON_MARK:
            //找到冒号结点，则折叠子表达式，作为冒号结点的右树
            mergeNode = mergeFormula(subFormulaNode);
            if(mergeNode == NULL)
            {
                foreach (FormulaNode *node, subFormulaNode)
                {
                    delete node;
                }
                return -2;
            }
            else
            {
                //子表达式折叠正确，则将折叠后的表达式，作为前冒号结点的右树
                nodeStack.last()->setRightNode(mergeNode);
                //然后折叠前问号表达式
                if(this->mergeQuestionEndFormula(nodeStack) == 0)
                {
                    //更新subFormulaNode
                    subFormulaNode.clear();
                }
                else
                {
                    return -3;
                }
            }
            break;
        default:
            subFormulaNode.prepend(nodeStack.takeLast());
            break;
        }
    }
    
    // '(' and ')' are not match
    return -4;
}

int FormulaCalculator::mergeQuestionMarkFormula(QList<FormulaNode *> &nodeStack)
{
    QList <FormulaNode *> subFormulaNode;
    FormulaNode *mergeNode = NULL;
    FormulaNode *questionNode = NULL;

    //找到问号、冒号或左括号，然后折叠子表达式，插入问号结点
    while(! nodeStack.isEmpty())
    {
        if((nodeStack.last()->type == FNT_QUESTION_MARK) ||
           (nodeStack.last()->type == FNT_COLON_MARK) ||
           (nodeStack.last()->type == FNT_LEFT_BRACKET))
        {
            break;
        }
        else
        {
            subFormulaNode.prepend(nodeStack.takeLast());
        }
    }

    if(subFormulaNode.length() > 0)
    {
        //找到了子表达式
        mergeNode = mergeFormula(subFormulaNode);
        if(mergeNode != NULL)
        {
            //子表达式折叠正确，则插入问号结点
            questionNode = new FOperNode(FOT_QUESTION_MARK);
            questionNode->setLeftNode(mergeNode);
            nodeStack.append(questionNode);
            return 0;
        }
    }

    //走到这里，或者是问号前面没有子表达式，或者是该子表达式折叠出错了。
    foreach (FormulaNode *node, subFormulaNode)
    {
        delete node;
    }

    return -1;
}

int FormulaCalculator::mergeColonMarkFormula(QList<FormulaNode *> &nodeStack)
{
    QList <FormulaNode *> subFormulaNode;
    FormulaNode *mergeNode = NULL;
    FormulaNode *colonNode = NULL;

    //找到问号、冒号或左括号，然后折叠子表达式，插入问号结点
    while(! nodeStack.isEmpty())
    {
        switch (nodeStack.last()->type)
        {
        case FNT_QUESTION_MARK:
            //找到最近的问题，折叠两者间的子表达式
            if(subFormulaNode.length() > 0)
            {
                //折叠子表达式
                mergeNode = mergeFormula(subFormulaNode);
                if(mergeNode != NULL)
                {
                    //子表达式折叠正确，则插入冒号结点
                    colonNode = new FOperNode(FOT_COLON_MARK);
                    colonNode->setLeftNode(mergeNode);
                    nodeStack.append(colonNode);

                    return 0;
                }
                else
                {
                    goto MergeColonMarkFormulaError;
                }
            }
            break;
        case FNT_COLON_MARK:
            //找到最近的是冒号，则折叠之前的问号表达式
            if(subFormulaNode.length() > 0)
            {
                //折叠子表达式
                mergeNode = mergeFormula(subFormulaNode);
                if(mergeNode != NULL)
                {
                    //子表达式折叠正确，则将折叠后的表达式，作为前冒号结点的右树
                    nodeStack.last()->setRightNode(mergeNode);
                    //然后折叠前问号表达式
                    if(this->mergeQuestionEndFormula(nodeStack) == 0)
                    {
                        //添加当前冒号结点
                        colonNode = new FOperNode(FOT_COLON_MARK);
                        nodeStack.append(colonNode);

                        return 0;
                    }
                    else
                    {
                        return -1;
                    }
                }
                else
                {
                    goto MergeColonMarkFormulaError;
                }
            }
            break;
        case FNT_LEFT_BRACKET:
            //找到最近的是左括号，则说明表达式出错了
            goto MergeColonMarkFormulaError;
            break;
        default:
            //其它情况，添加进子表达式
            subFormulaNode.prepend(nodeStack.takeLast());
            break;
        }
    }

MergeColonMarkFormulaError:
    //走到这里，子表达式折叠出错了。
    foreach (FormulaNode *node, subFormulaNode)
    {
        delete node;
    }

    return -1;
}

int FormulaCalculator::mergeQuestionEndFormula(QList<FormulaNode *> &nodeStack)
{
    FormulaNode *lastNode = NULL;
    FormulaNode *secondLastNode = NULL;

    while(nodeStack.length() > 1)
    {
        lastNode = nodeStack.takeLast();
        secondLastNode = nodeStack.last();

        if((lastNode->type == FNT_COLON_MARK) &&
           (secondLastNode->type == FNT_QUESTION_MARK))
        {
            //最后结点是冒号运算符，倒数第二个结点是问题运算符，将最后冒号运算符作为问题运算符的右树
            secondLastNode->setRightNode(lastNode);
            secondLastNode->type = FNT_SUB_FORMUAL;
        }
        else if((lastNode->type == FNT_SUB_FORMUAL) &&
                (secondLastNode->type == FNT_COLON_MARK))
        {
            //最后结点是子运算符，倒数第二个结点是冒号运算符，将最后子运算符作为冒号运算符的右树
            if(secondLastNode->setRightNode(lastNode) < 0)
            {
                qDebug("FormulaCalculator::mergeQuestionEndFormula: merge CS error.");
                delete lastNode;
                return -1;
            }
        }
        else if((lastNode->type == FNT_COLON_MARK) &&
                (secondLastNode->type == FNT_SUB_FORMUAL))
        {
            //最后结点是冒号运算符，倒数第二个结点是子运算符，将最后子运算符作为冒号运算符的左树
            if(lastNode->setLeftNode(secondLastNode) < 0)
            {
                qDebug("FormulaCalculator::mergeQuestionEndFormula: merge SC error.");
                delete lastNode;
                return -2;
            }
            nodeStack.takeLast();
            nodeStack.append(lastNode);
        }
        else
        {
            //其它情况，认为运表达式还没解析全，退出
            nodeStack.append(lastNode);
            break;
        }
    }

    return 0;
}

int FormulaCalculator::mergeFormulaToOneNode(QList<FormulaNode *> &nodeStack)
{
    QList <FormulaNode *> subFormulaNode;
    FormulaNode *mergeNode = NULL;

    //从后往前找子表达式，然后折叠子表达式
    while(! nodeStack.isEmpty())
    {
        switch (nodeStack.last()->type)
        {
        case FNT_LEFT_BRACKET:
            //找到左括号，表示出错了
            foreach (FormulaNode *node, subFormulaNode)
            {
                delete node;
            }
            return -1;
            break;
        case FNT_COLON_MARK:
            //找到冒号结点，则折叠子表达式，作为冒号结点的右树
            mergeNode = mergeFormula(subFormulaNode);
            if(mergeNode == NULL)
            {
                foreach (FormulaNode *node, subFormulaNode)
                {
                    delete node;
                }
                return -2;
            }
            else
            {
                //子表达式折叠正确，则将折叠后的表达式，作为前冒号结点的右树
                nodeStack.last()->setRightNode(mergeNode);
                //然后折叠前问号表达式
                if(this->mergeQuestionEndFormula(nodeStack) == 0)
                {
                    //更新subFormulaNode
                    subFormulaNode.clear();
                }
                else
                {
                    return -3;
                }
            }
            break;
        default:
            subFormulaNode.prepend(nodeStack.takeLast());
            break;
        }
    }

    if(subFormulaNode.length() > 0)
    {
        mergeNode = mergeFormula(subFormulaNode);
        if(mergeNode == NULL)
        {
            return -4;
        }
        else
        {
            nodeStack.append(mergeNode);
            return 0;
        }
    }

    //表达式无内容
    return -5;
}

FormulaNode *FormulaCalculator::mergeFormula(QList<FormulaNode *> &nodeStack)
{
    FormulaNode *subNode = NULL;

    if(nodeStack.length() <= 0)
    {
        return NULL;
    }

    //从高到低的优先级遍历列表中的所有运算符，如果找到了，则折叠该运算符前后的元素为该
    //运算符的左右子树
    for(int order = FPT_VALUE + 1; order < FPT_MAX; order++)
    {
        for(int i = 0; i < nodeStack.length();)
        {
            subNode = nodeStack.at(i);
            if((subNode->type == FNT_OPERATOR) &&
               (subNode->priority == order))
            {
                subNode->setLeftNode(nodeStack.at(i - 1));
                subNode->setRightNode(nodeStack.at(i + 1));
                subNode->type = FNT_SUB_FORMUAL;

                nodeStack.takeAt(i + 1);
                nodeStack.takeAt(i - 1);
            }
            else
            {
                i++;
            }
        }
    }

    return nodeStack.first();
}

FormulaNode *FormulaCalculator::parseSignalNode(const QByteArray &formulaStr, int &index)
{
    FormulaNode *node = NULL;
    int startPos = formulaStr.indexOf("(", index) + 1;
    int endPos = formulaStr.indexOf(")", index);
    if((startPos < 0) || (endPos < 0))
    {
        return NULL;
    }
    
    QByteArray sigStr = formulaStr.mid(startPos, endPos - startPos);
    QList<QByteArray> sigStrList = sigStr.split(',');
    
    if(sigStrList.length() == 2)
    {
        node = new FValueNode(sigStrList.at(0).toUInt(),
                              0,
                              sigStrList.at(1).toUInt());
    }
    else if(sigStrList.length() == 3)
    {
        node = new FValueNode(sigStrList.at(0).toUInt(),
                              sigStrList.at(1).toUInt(),
                              sigStrList.at(2).toUInt());
    }
    else if(sigStrList.length() == 4)
    {
        node = new FValueNode(sigStrList.at(0).toUInt(),
                              sigStrList.at(1).toUInt(),
                              sigStrList.at(2).toUInt(),
                              sigStrList.at(3).toUInt());
    }
    else
    {
        node = NULL;
    }

    index = endPos;

    return node;
}

FormulaNode *FormulaCalculator::parseValueNode(const QByteArray &formulaStr, int &index)
{
    bool ok = false;
    int i;
    
    for(i = index; i < formulaStr.length(); ++i)
    {
        if(isdigit(formulaStr.at(i)) || (formulaStr.at(i) == '.'))
        {
            continue;
        }
        else
        {
            break;
        }
    }
    
    QByteArray valueStr = formulaStr.mid(index, i - index);
    float value = valueStr.toFloat(&ok);
    if(! ok)
    {
        return NULL;
    }
    else
    {
        index = i - 1;
        return new FValueNode(value);
    }
}
