#include "ui_formulachecker.h"
#include "ui_ui_formulachecker.h"
#include "formulacalculator.h"
#include "MsgHandlerWapper.h"

UI_FormulaChecker::UI_FormulaChecker(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UI_FormulaChecker)
{
    ui->setupUi(this);

    connect(ui->pbcheck, SIGNAL(clicked(bool)), this, SLOT(onCheck()));
    connect(MsgHandlerWapper::instance(), SIGNAL(message(QtMsgType,QString)), this, SLOT(onNewLog(QtMsgType,QString)));
}

UI_FormulaChecker::~UI_FormulaChecker()
{
    delete ui;
}

void UI_FormulaChecker::onCheck()
{
    QString formula = ui->leformula->text();
    FormulaCalculator fc;
    FormulaNode *formulaNode = fc.parseFormula(formula);
    if(formulaNode != NULL)
    {
        ui->telog->append(QString("UI_FormulaChecker::onCheck: \"%1\" = %2").arg(formula)
                          .arg(formulaNode->calc()));
        delete formulaNode;
    }
}

void UI_FormulaChecker::onNewLog(QtMsgType type, const QString &msg)
{
    ui->telog->append(msg);
    (void)type;
}
