#-------------------------------------------------
#
# Project created by QtCreator 2017-06-21T09:50:39
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FormulaChecker
TEMPLATE = app


SOURCES += main.cpp\
        ui_formulachecker.cpp \
    formulacalculator.cpp \
    MsgHandlerWapper.cpp

HEADERS  += ui_formulachecker.h \
    formulacalculator.h \
    MsgHandlerWapper.h

FORMS    += ui_formulachecker.ui
